v1.0.10 (Apr 14, 2017)
 -----------------------------
 - Replaced deprecated WooCommerce functions
 - Added support for new WooCommerce Zoom & Slider effect in the single products
 - Improved Single products styling
 
 Files updated:
- functions.php
- style.css
- css/woocommerce2.css
- woocommerce/archive-product.php
- woocommerce/single-product.php

v1.0.9 (Apr 06, 2017)
-----------------------------
- Include Support tab in Theme Options Panel
- Fixed Schema Issue
- Fixed wrong background field constructor args in the Options Panel

Files updated:
- style.css
- functions.php
- theme-options.php
- options/support.php
- options/options.php 
- options/css/options.css
- options/js/clipboard.min.js
- options/js/help.js
- options/fields/background/field_background.php 
- options/fields/group/field_group.php 

v1.0.8 (Mar 03, 2017)
-----------------------------
- One click demo importer improvements

Files updated:
- functions.php
- lang/socialme.pot
- style.css

v1.0.7 (Jan 18, 2017)
-----------------------------
- One click demo importer improvements

Files updated:
- style.css
- functions.php
- options/demo-importer/


v1.0.6 (Nov 14, 2016)
-----------------------------
- Improved lazy loading support
- Fixed "Author" label appearing on child comments of an author comment
- Function "mts_like_dislike" is now overridable via child theme
- Updated translation template

Files updated:
- functions.php
- lang/default.pot
- post-format/format-gallery.php
- post-format/format.php
- style.css

v1.0.5 (Oct 22, 2016)
-----------------------------
- One click demo importer improvements
- Install Plugins page improvements

Files updated:
- style.css
- functions.php
- theme-presets.php
- options/
- lang/
- functions/plugin-activation.php


v1.0.4 (Oct 6, 2016)
-----------------------------
- Fixed bug which was not showing right aligned images in single posts

Files updated:
- style.css


v1.0.3 (May 27, 2016)
-----------------------------
- Fixed missing circle-output.js file on free version of WP Review plugin

Files updated:
- style.css
- functions.php


v1.0.2 (May 23, 2016)
-----------------------------
- Fixed Facebook share mobile issue
- Removed unused Facebook like option
- Fixed author posts widget issue

Files updated:
- style.css
- functions.php
- theme-options.php
- functions/theme-actions.php
- functions/widget-authorposts.php


v1.0.1 (May 05, 2016)
-----------------------------
- Fixed demo importer menus issue
- Fixed BloggingTips theme options import issue

Files updated:
- style.css
- functions.php
- theme-presets.php
- options/demo-importer/demo-files/bloggingtips/content.xml
- options/demo-importer/demo-files/bloggingtips/widgets.json
- options/demo-importer/demo-files/bloggingtips/theme_options.txt


v1.0 (May 03, 2016)
-----------------------------
Theme Released
